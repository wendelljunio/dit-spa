import axios from 'axios';

const url = 'https://localhost:5001/v1';

export const client = axios.create({
	baseURL: url,
	headers: {
		'Content-Type': 'application/json'
	}
});
