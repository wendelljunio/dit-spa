import React, { Component } from 'react';
import { client } from '../../client/client';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import * as productActions from './actions';
import Card from '../../components/Card';
import Table from '../../components/Table';

class Product extends Component {
  constructor(props) {
    super(props);
    this.editProduct = this.editProduct.bind(this);
    this.removeProduct = this.removeProduct.bind(this);
  }
  
  editProduct(productId) {
    this.props.setEditProductId(productId)
    this.props.history.push('/editar')
  }

  removeProduct(productId) {
    client.delete('/products/' + productId)
    .then(response => {
      const newProduct = this.props.products.filter(p => {
        return p.id !== productId
      })
      this.props.setProducts(newProduct)
      alert("Removido com sucesso")
    })
    .catch(e => {
      console.log(e)
    })
  }

  componentDidMount(){
    client.get('/products')
      .then(response => {
        console.log(response.data)
        this.props.setProducts(response.data)
      })
      .catch(e => {
        console.log(e)
      })
  }

  render() {
    return (
      <div>
        <div>
          <div className="d-flex justify-content-start">
            <h1>Produtos cadastrados</h1>
          </div>
          
        </div>
        <Card>
          <div className="d-flex justify-content-end mb-3">
            <Link to="/cadastrar" className="btn btn-primary">Cadastrar</Link>
          </div>
            <Table tablebordered="table-bordered" tablehover="table-hover" sm="table-sm" tablestriped="table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nome</th>
                  <th>Quantidade</th>
                  <th>Preço</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {this.props.products.map(product => 
                <tr key={product.id}>
                  <td>{product.id}</td>
                  <td>{product.name}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price}</td>
                  <td>
                    <button className="btn btn-sm btn-outline-primary mr-2" onClick={() => this.editProduct(product.id)}>Editar</button>
                    <button className="btn btn-sm btn-outline-danger" onClick={() => this.removeProduct(product.id)}>remover</button>
                  </td>
                </tr>
                  )
                }
              </tbody>
            </Table>
            </Card>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  products: store.product.products,
  editProductId: store.product.editProductId
});

const mapDispatchToProps = dispatch => ({
  setProducts: (value) => dispatch(productActions.setProducts(value)),
  setEditProductId: (value) => dispatch(productActions.setEditProductId(value))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Product));