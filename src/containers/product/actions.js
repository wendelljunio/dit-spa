export const SET_PRODUCTS = 'product/SET_PRODUCTS'
export const SET_EDIT_PRODUCT_ID = 'product/SET_EDIT_PRODUCT_ID'

export const setProducts = payload => {
  return {
    type: SET_PRODUCTS,
    payload
  }
}

export const setEditProductId = payload => {
  return {
    type: SET_EDIT_PRODUCT_ID,
    payload
  }
}