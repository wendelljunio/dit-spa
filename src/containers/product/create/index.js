import React, { Component } from 'react';
import { client } from '../../../client/client';
import Card from '../../../components/Card';
import AlertDanger from '../../../components/Alert/alertDanger';

class ProductCreate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      productCreate: {},
      errors: null
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { productCreate } = { ...this.state };
    const currentProduct = productCreate;
    currentProduct[name] = value;
    this.setState({productCreate: currentProduct });
  }

  handleSubmit(event) {
    event.preventDefault();
    const prod = {
      name: this.state.productCreate.name,
      price: this.state.productCreate.price ? parseFloat(this.state.productCreate.price) : 0,
      quantity: this.state.productCreate.quantity ? parseInt(this.state.productCreate.quantity) : 0
    }
    client.post('/products/', prod)
    .then(response => {
      console.log(response.data)
      this.props.history.push('/produtos')
    })
    .catch(e => {
      this.setState({errors: e.response.data})
    })
  }

  render() {
    return(
      <div className="row">
        <div className="col-8">
        <h1>Cadastrar produto</h1>
        <Card>
            { this.state.errors ?
              <AlertDanger>
                {this.state.errors}
              </AlertDanger>
              : <></>
            }
            <form onSubmit={this.handleSubmit}>
              <div className="form-group row">
                <label htmlFor="name" className="col-sm-2 col-form-label">Nome:</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" name="name" id="name" value={this.state.productCreate.name} onChange={this.handleChange}/>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="price" className="col-sm-2 col-form-label">Preço:</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" name="price" id="price" value={this.state.productCreate.price} onChange={this.handleChange}/>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="quantity" className="col-sm-2 col-form-label">Quantidade:</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" name="quantity" id="quantity" value={this.state.productCreate.quantity} onChange={this.handleChange}/>
                </div>
              </div>
              <div className="row">
                <div className="offset-2 col-10">
                  <button type="submit" className="btn btn-outline-primary btn-block">Salvar</button>
                </div>
              </div>
            </form>
          </Card>
        </div>
      </div>
    )
  }
}

export default (ProductCreate);