import { SET_PRODUCTS, SET_EDIT_PRODUCT_ID } from './actions'

const initialState = {
  products: [],
  editProductId: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return { ...state, products: action.payload };
    case SET_EDIT_PRODUCT_ID:
      return { ...state, editProductId: action.payload };
    default:
      return state;
  }
}