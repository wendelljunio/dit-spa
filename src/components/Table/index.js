import React from 'react';

const Table = (props) => {
  return (
    <table className={`table ${props.tablestriped}  ${props.tablebordered} ${props.tablehover} ${props.sm}`}>
        {props.children}
    </table>
  );
}

export default Table;