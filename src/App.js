import React from 'react';
import { Route, Switch } from "react-router-dom";
import Product from "./containers/product";
import ProductEdit from "./containers/product/edit";
import ProductCreate from "./containers/product/create";
import NavBar from "./components/NavBar";

function App() {
  return (
    <div>
      <header className="App-header">
        <NavBar title="DTI"></NavBar>
      </header>
      <main>
        <div className="container">
          <Switch>
            <Route path="/produtos" component={Product} />
            <Route path="/editar" component={ProductEdit} />
            <Route path="/cadastrar" component={ProductCreate} />
          </Switch>
        </div>
      </main>
    </div>
  );
}

export default App;
