import React, { Component } from 'react';
import { client } from '../../../client/client';
import { connect } from 'react-redux';
import Card from '../../../components/Card';
import AlertDanger from '../../../components/Alert/alertDanger';

class ProductEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      productEdit: {},
      errors: null
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { productEdit } = { ...this.state };
    const currentProduct = productEdit;
    currentProduct[name] = value;
    this.setState({productEdit: currentProduct });
  }

  handleSubmit(event) {
    event.preventDefault();
    const prod = {
      id: this.state.productEdit.id,
      name: this.state.productEdit.name,
      price: this.state.productEdit.price ? parseFloat(this.state.productEdit.price) : 0,
      quantity: this.state.productEdit.quantity ? parseInt(this.state.productEdit.quantity) : 0
    }
    client.put('/products/' + this.props.editProductId, prod)
    .then(response => {
      console.log(response.data)
      this.props.history.push('/produtos')
    })
    .catch(e => {
      this.setState({errors: e.response.data})
    })

  }

  componentDidMount(){
    if(this.props.editProductId){
      client.get('/products/' + this.props.editProductId)
      .then(response => {
        console.log(response.data)
        this.setState({productEdit: response.data})
      })
      .catch(e => {
        console.log(e)
      })
    } else {
      this.props.history.push('/produtos')
    }
    
  }
  renderAlert = (errors) => 
    <AlertDanger>
      {errors}
    </AlertDanger>
  render() {
    
    return(
      <div className="row">
        <div className="col-8">
          <h1>Editar produto</h1>
          <Card>
            { 
              this.state.errors ? this.renderAlert(this.state.errors) : <></>
            }
            <form onSubmit={this.handleSubmit}>
              <div className="form-group row">
                <label htmlFor="name" className="col-sm-2 col-form-label">Nome:</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" name="name" id="name" value={this.state.productEdit.name} onChange={this.handleChange}/>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="price" className="col-sm-2 col-form-label">Preço:</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" name="price" id="price" value={this.state.productEdit.price} onChange={this.handleChange}/>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="quantity" className="col-sm-2 col-form-label">Quantidade:</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control" name="quantity" id="quantity" value={this.state.productEdit.quantity} onChange={this.handleChange}/>
                </div>
              </div>
              <div className="row">
                  <div className="offset-2 col-10">
                    <button type="submit" className="btn btn-outline-primary btn-block">Salvar</button>
                  </div>
                </div>
            </form>
            </Card>
        </div>
      </div>
    )
  }
}

const mapStateToProps = store => ({
  editProductId: store.product.editProductId
});

export default connect(mapStateToProps)(ProductEdit);