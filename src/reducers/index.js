import { combineReducers } from 'redux';
import product from '../containers/product/reducer';

export default combineReducers({
    product
});