import React from 'react';

const AlertDanger = (props) => {
	return (
		<div className="alert alert-danger">
      <p>{props.children.Name}</p>
			<p>{props.children.Price}</p>
			<p>{props.children.Quantity}</p>
		</div>
	);
}

export default AlertDanger;